public class Sheep {

   enum Animal {sheep, goat}

   public static void main (String[] param) {
      // for debugging
   }

   public static void reorder (Animal[] animals) {
      /* Solution inspired by StackOverflow (https://stackoverflow.com/a/25852710).
      Didn't use .ordinal, since im not familiar with it. */
      int first = 0;
      int last = animals.length - 1;

      while(first < last){
         while(animals[first].equals(Animal.goat) && first < last){
            ++first;
         }
         while(animals[last].equals(Animal.sheep) && first < last){
            --last;
         }
         if (first < last){
            Animal temp = animals[first];
            animals[first] = animals[last];
            animals[last] = temp;
            ++first;
            --last;
         }

      }
   }
}